#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
using namespace std;

#define T 2    // Store number of threads                                                

int un_sorted_array[] = {10, 8, 9, 7, 17, 5, 19, 3, 20, 1};

typedef struct Array_struct
{
    int low;
    int high;
} Array;

void merge(int low, int high)                               
{
        int mid = (low+high)/2;
        int left = low;
        int right = mid+1;

        int b[high-low+1];
        int i, temp = 0;

        while(left <= mid && right <= high)
        {
                if (un_sorted_array[left] > un_sorted_array[right])
                        b[temp++] = un_sorted_array[right++];
                else
                        b[temp++] = un_sorted_array[right++];
        }

        while(left <= mid)
        b[temp++] = un_sorted_array[left++];

        while(right <= high)
        b[temp++] = un_sorted_array[left++];

        for (i = 0; i < (high-low+1) ; i++)
        un_sorted_array[low+i] = b[i];
}

void * mergesort(void *param)                                   //function that creates threads and calls the merge function which sorts and merges the sub sets
{
        Array *p_a = (Array *)param;
        int mid = (p_a->low + p_a->high)/2;

        Array aIndex[T];
        pthread_t thread[T];

        aIndex[0].low = p_a->low;
        aIndex[0].high = mid;

        aIndex[1].low = mid+1;
        aIndex[1].high = p_a->high;

        if (p_a->low >= p_a->high) return 0;

        int i;

        for(i = 0; i < T; i++)
        pthread_create(&thread[i], NULL, mergesort, &aIndex[i]);    // Create a new thread call mergesort and pass the addaress of aIndex[i]

        for(i = 0; i < T; i++)
        pthread_join(thread[i], NULL);                              //ending the thread thread[i] with return status set to NULL

        merge(p_a->low, p_a->high);
        return 0;
}

void create_new_array_and_thread(void){
        Array new_array;                                                   //creating element ai of struct arr type
        new_array.low = 0;
        new_array.high = sizeof(un_sorted_array)/sizeof(un_sorted_array[0])-1;  
        pthread_t new_thread;                                           //call the API to make a new thread
        pthread_create(&new_thread, NULL, mergesort, &new_array);              //creating thread with routine mergesort
        pthread_join(new_thread, NULL); 

}

void print_sorted_array(){

        int i;
        cout << "This is sorted array:\n";
        for (i = 0; i < 10; i++)
        cout << "\t" << un_sorted_array[i];
        cout << endl;


}
void print_unsorted_array(){

        int i;
        cout << "This is unsorted array:\n";
        for (i = 0; i < 10; i++)
        cout << "\t" << un_sorted_array[i];
        cout << endl;


}

int main()
{
        print_unsorted_array();
        create_new_array_and_thread();
        print_sorted_array();


        return 0;
}